import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import HomeIcon from '@material-ui/icons/Home';
import MenuItem from '@material-ui/core/MenuItem';
import GetAppIcon from '@material-ui/icons/GetApp';
import Menu from '@material-ui/core/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  installResources: {
    margin:2,
  }
}));

export default function TopBar(props) {
  const classes = useStyles();
//  const [auth, setAuth] = React.useState(true);
  const [auth] = React.useState(true);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

/*
  const handleChange = event => {
    setAuth(event.target.checked);
  };
*/
  const handleMenu = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  function Home(where) {
    var browserHistory = props.history;
    browserHistory.push("/"+where);
  }

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <IconButton edge="start" className={classes.menuButton} color="inherit" onClick={() => Home('')} aria-label="Menu">
            <HomeIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            {props.page}
          </Typography>
          {/* <Button color="inherit">Return to App Page</Button> */}
          {auth && (
            <div>
              <IconButton
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={handleMenu}
                color="inherit"
              >
                <AccountCircle />
              </IconButton>
              <Menu
                id="menu-appbar"
                anchorEl={anchorEl}
                anchorOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                keepMounted
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                open={open}
                onClose={handleClose}
              >
                <MenuItem onClick={() => Home('LogIn')}>LogIn</MenuItem>
                <MenuItem onClick={handleClose}>LogOut</MenuItem>
              </Menu>
            </div>
          )}
          <IconButton edge="start" className={classes.installResources} color="inherit" onClick={() => Home('Installers/')} aria-label="Install Resources">
            <GetAppIcon />
          </IconButton>
        </Toolbar>
      </AppBar>
    </div>
  );
}
