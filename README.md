# ReImagine client

A simple electron app to anonimize and upload medical data

## Binary installation of latest version (recommended)
* [Windows](https://services.simbiosys.upf.edu/reimagine/reImagineClient%20Setup%201.1.0-master.exe?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIOSFODNN7EXAMPLE%2F20200414%2F%2Fs3%2Faws4_request&X-Amz-Date=20200414T184850Z&X-Amz-Expires=604800&X-Amz-SignedHeaders=host&X-Amz-Signature=23e3bf41a478429a46aa9c82f64bee34fa14e3d1f3155b8f421c874dcdd499a4)
* [Linux](https://services.simbiosys.upf.edu/reimagine/reImagineClient%201.1.0-master.AppImage?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIOSFODNN7EXAMPLE%2F20200414%2F%2Fs3%2Faws4_request&X-Amz-Date=20200414T185016Z&X-Amz-Expires=604800&X-Amz-SignedHeaders=host&X-Amz-Signature=736f7a29db307f925eb6f1f2765578778dbcfe955eb860717be8af82be8fb01d) 
* Mac: Not available at the moment

## Installation of the development version (only for developers)
Clone the repo:

Then install dependencies and run the project
 * Node >= 8.2.0

```bash
$ yarn install
$ yarn electron-dev
```

## Packaging the app
* `yarn electron-pack


Your app will packaged as an .exe in the dist folder using the [electron-builder](https://github.com/electron-userland/electron-builder) package. Edit the build section of the package.json to build on additional platforms.



# Funcionalidad api
Esta implementada con express y has 2 endpoints declarados en el archivo things.js dentro de src. Funciona en desarrollo ya que se ejecuta el localhost pero en produccion no.

